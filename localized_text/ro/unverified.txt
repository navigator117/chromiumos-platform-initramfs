Imaginea de pe suportul de recuperare este o imagine de dezvoltare neverificată.

Recuperarea va continua, deoarece pornirea verificată este dezactivată.
Dacă ulterior activaţi din nou verificarea, va trebui să repetaţi
recuperarea utilizând o imagine oficială a sistemului de operare Chrome.
