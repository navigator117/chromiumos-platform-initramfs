Używasz nieaktualnego, oficjalnego obrazu przywracania systemu operacyjnego Chrome.

Przywracanie będzie kontynuowane, ponieważ sprawdzanie systemu jest wyłączone.
Jeśli później ponownie włączysz sprawdzanie systemu, trzeba będzie powtórzyć
przywracanie przy użyciu aktualnego obrazu systemu operacyjnego Chrome.
