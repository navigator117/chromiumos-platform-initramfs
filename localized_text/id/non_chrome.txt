Komputer Anda tampaknya tidak menggunakan sistem Chrome OS!

Operasi ini akan menghapus sistem operasi lainnya yang mungkin telah Anda
pasang serta semua data yang ada; tidak ada lagi kesempatan untuk mencadangkan.
Jika Anda memiliki data apa pun di komputer ini yang ingin dipertahankan, Anda
harus membatalkan pemulihan.
