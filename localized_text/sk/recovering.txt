Prebieha obnovenie systému...

Táto operácia môže trvať niekoľko minút. Nevypínajte počítač ani ho neodpájajte
od zdroja napájania.
