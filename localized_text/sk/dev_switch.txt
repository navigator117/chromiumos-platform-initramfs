Verifikácia operačného systému Chrome OS je vypnutá.

Ak to nie je úmyselné, zrušte obnovenie a zakážte
režim pre vývojárov, čím verifikáciu systému povolíte.  Viac informácií nájdete na tejto
adrese URL:

https://www.google.com/chromeos/recovery
