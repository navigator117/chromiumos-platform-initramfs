Izskatās, ka jūsu datorā netiek izmantota sistēma Chrome OS!

Izpildot šo darbību, tiks dzēsta instalētā operētājsistēma un visi dati. Dublējumu nevarēs izveidot.
Ja šajā datorā glabājat kādus datus, kurus nevēlaties dzēst, atceliet sistēmas atkopšanu.
