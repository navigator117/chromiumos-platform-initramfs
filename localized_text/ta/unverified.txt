உங்கள் மீட்டெடுப்பு மீடியாவில் உள்ள படமானது சரிபார்க்கப்படாத டெவலப்பர் படமாக உள்ளது.

சரிபார்ப்பு நிறுத்தம் முடக்கப்பட்டுள்ளதால் மீட்டெடுப்புத் தொடரும்.
நீங்கள் மீண்டும் சரிபார்ப்பிற்குத் திரும்பினால், புதுப்பித்த நிலையில் உள்ள அதிகாரப்பூர்வ Chrome OS படத்துடன் நீங்கள் மீண்டும் மீட்டெடுப்பை நிகழ்த்த வேண்டும்.
