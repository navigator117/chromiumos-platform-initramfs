Hindi lumilitaw ang iyong computer bilang Chrome OS system!

Tatanggalin ng pagpapatakbong ito ang anumang iba pang operating system na maaaring na-install mo at lahat ng data; walang pagkakataon para sa backup.
Kung mayroon kang anumang data sa computer na ito na nais mong panatilihin, dapat mong kanselahin ang pagbawi.
