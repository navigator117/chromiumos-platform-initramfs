Billedet på dit genoprettelsesmedie er ikke et bekræftet udviklerbillede.

Genoprettelsen fortsætter, fordi bekræftet opstart er deaktiveret.
Hvis du senere aktiverer bekræftelsen igen, skal du gentage
genoprettelsen med et officielt Chrome OS-billede.
